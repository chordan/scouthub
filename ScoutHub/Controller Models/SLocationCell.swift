//
//  SLocationCell.swift
//  ScoutHub
//
//  Created by Jordan Larock on 2017-12-15.
//  Copyright © 2017 Jordan Larock. All rights reserved.
//

import UIKit

class SLocationCell: UICollectionViewCell {
    
    @IBOutlet var locationImage: UIImageView!
    @IBOutlet var locationLabel: UILabel!
    
    func displayCell(image: UIImage, name: String){
        locationImage.image = image
        locationLabel.text = name
    }
    
}

