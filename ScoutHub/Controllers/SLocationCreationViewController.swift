//
//  SLocationCreationViewController.swift
//  ScoutHub
//
//  Created by Jordan Larock on 2018-01-15.
//  Copyright © 2018 Jordan Larock. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import Foundation

let fullName    = "First Last"
let fullNameArr = fullName.components(separatedBy: " ")

    class SLocationCreationViewController: UIViewController{
    
        var ref: DatabaseReference?
        
        @IBOutlet weak var nameTxt: UITextField!
        
        @IBOutlet weak var descTxt: UITextField!
        
        @IBOutlet weak var tagsTxt: UITextField!
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            // Do any additional setup after loading the view, typically from a nib.
            ref = Database.database().reference()
            navigationItem.title = "Location Creation"
            
        }
        
        @IBAction func okBtnPressed(_ sender: Any) {
            
            if ((!(nameTxt.text?.isEmpty)!) && (!(descTxt.text?.isEmpty)!) && (!(tagsTxt.text?.isEmpty)!)){
                
                let tagsArray = tagsTxt.text?.components(separatedBy: " ")
                
                let locationToAdd = ScoutedLocation(name: nameTxt.text!, likes: 0, comments: ["",""], description: descTxt.text!, tags: tagsArray!, coordinates: 0.0)
                
                let data = [
                    "name": locationToAdd.name,
                    "likes": locationToAdd.likes,
                    "comments": locationToAdd.comments,
                    "description": locationToAdd.description,
                    "tags": locationToAdd.tags,
                    "coordinates": locationToAdd.coordinates
                    ] as [String : AnyObject]
                
                
                self.ref?.child("locations").childByAutoId().setValue(data)
            }
        }
        
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.view.endEditing(true)
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
}
