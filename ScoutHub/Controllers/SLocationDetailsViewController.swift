//
//  SLocationDetailsViewController.swift
//  ScoutHub
//
//  Created by Jordan Larock on 2017-12-18.
//  Copyright © 2017 Jordan Larock. All rights reserved.
//

import UIKit


class SLocationDetailsViewController: UIViewController {
    
    var selectedCellDetails = ScoutedLocation()
   
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var nameText: UILabel!
    @IBOutlet weak var likesText: UILabel!
    @IBOutlet weak var tagsText: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        navigationItem.title = selectedCellDetails.name
        nameText.text = selectedCellDetails.name
        descriptionText.text = selectedCellDetails.description
        likesText.text = "Likes: \(selectedCellDetails.likes)"
        
        var tagList = "#\(selectedCellDetails.tags.joined(separator: ", #"))"
        
        tagsText.text = tagList
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
