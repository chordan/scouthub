//
//  ScoutedLocation.swift
//  ScoutHub
//
//  Created by Jordan Larock on 2017-12-15.
//  Copyright © 2017 Jordan Larock. All rights reserved.
//
    
class ScoutedLocation: Codable {
        var name: String
        let likes: Int
        let comments: [String]
        let description: String
        let tags: [String]
        let coordinates: Float

        init (name: String, likes: Int, comments: [String],
              description: String, tags: [String], coordinates: Float) {
            self.name = name
            self.likes = likes
            self.comments = comments
            self.description = description
            self.tags = tags
            self.coordinates = coordinates
        }
        init (){
            self.name = "name"
            self.likes = 0
            self.comments = ["comm1"]
            self.description = "desc"
            self.tags = ["tag1"]
            self.coordinates = 0.0
        }

    }

