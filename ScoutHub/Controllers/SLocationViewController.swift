//
//  SecondViewController.swift
//  ScoutHub
//
//  Created by Jordan Larock on 2017-10-06.
//  Copyright © 2017 Jordan Larock. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class SLocationViewController: UICollectionViewController {

    var items = [ScoutedLocation]()
    var selectedCell = ScoutedLocation()
    var ref: DatabaseReference?
    var dataBaseHandle: DatabaseHandle?
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        items.append(contentsOf: [ScoutedLocation(name: "one", likes: 2, comments: ["ayy.","bruh"], description: "desk desc", tags: ["doot","B"], coordinates: 123), ScoutedLocation(name: "takeTheL", likes: 2, comments: ["comment.","otherComment"], description: "desk descw2", tags: ["boot","D"], coordinates: 123)])
        
        ref = Database.database().reference()
        
        dataBaseHandle = ref?.child("locations").observe(.childAdded, with: { (snapshot) in
            
            let locString = snapshot.value as? [String:AnyObject]
            
            let requestedLoc = ScoutedLocation(name: locString!["name"] as! String, likes: locString!["likes"] as! Int, comments: locString!["comments"] as! [String], description: locString!["description"] as! String, tags: locString!["tags"] as! [String], coordinates: locString!["coordinates"] as! Float)
                
                
            self.items.append(requestedLoc)
            
            
            self.collectionView?.reloadData()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // tell the collection view how many cells to make
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    // make a cell for each cell index path
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! SLocationCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cell.locationLabel.text = self.items[indexPath.item].name
        
        return cell
    }
    
    // UICollectionViewDelegate protocol
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
        performSegue(withIdentifier: "locDetailSegue", sender: indexPath.item)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailsController = segue.destination as? SLocationDetailsViewController {
            
                let cellNumber = sender as! Int
            
                detailsController.selectedCellDetails = items[cellNumber]
            }
    }
    
    
    
}

